import { shallowMount } from '@vue/test-utils'
import MyHelloWorld from '@/components/MyHelloWorld.vue'

describe('MyHelloWorld.vue', () => {
  it('renders the hard-coded test string', () => {
    const wrapper = shallowMount(MyHelloWorld)
    expect(wrapper.text()).toMatch("Hello World from Olof!")
  })
})
